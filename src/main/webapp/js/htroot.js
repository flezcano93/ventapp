var htroot = {};

htroot.dataTableMap = {};
htroot.suggestMap = {};
htroot.pickerMap = {};
htroot.formLoaderMap = {};
htroot.fieldValueSetterMap = {};
htroot.defaultRowMap = {};
htroot.dynamicFormListHelperMap = {};

htroot.capitalize = function(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

htroot.isBlank = function(value) {
	if (value == null) {
		return true;
	}
	if (typeof value === 'string') {
		return value == "null" || value.trim() == '';
	}
	return false;
}

htroot.coalesce = function(value, defaultValue) {
	return htroot.isBlank(value) ? defaultValue : value;
}

htroot.htmlSanitize = function(str) {
	var entityMap = {
		"&" : "&amp;",
		"<" : "&lt;",
		">" : "&gt;",
		'"' : '&quot;',
		"'" : '&#39;',
		"/" : '&#x2F;'
	};
	return String(str).replace(/[&<>"'\/]/g, function(s) {
		return entityMap[s];
	});
}

/**
 * @param value
 *            {'nombre':'Juan', 'apellido':'Perez<script>alert(1)</script>'}
 * @return {'nombre':'Juan',
 *         'apellido':'Perez&lt;script&gtalert(1)&lt;/script&gt'}
 * 
 */
htroot.objectSanitize = function(item, key, value) {
	if (value == null) {
		return;
	}
	if (value.constructor == Object) {
		item[key] = value;
		$.each(value, function(id, objValue) {
			htroot.objectSanitize(value, id, objValue);
		});
	} else {
		item[key] = htroot.htmlSanitize(value);
	}
}

htroot.addOverlay = function(divId) {
	var icon = '<div id="overlayDiv" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
	$("#" + divId).append(icon);
}

htroot.showOverlay = function() {
	htroot.addOverlay("contentBox");
}

htroot.hideOverlay = function() {
	$("#overlayDiv").remove();
}

htroot.numberWithCommas = function(x) {
	if (x == null) {
		return null;
	}
	x = x.replace(".", ",");
	var parts = x.split(",");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "\.");
	return parts.join(",");
}

htroot.formatNumber = function(str, separator) {
	separator = separator == null ? "," : separator;
	if (htroot.isBlank(str)) {
		return null;
	}
	return str.replace(/\B(?=(\d{3})+(?!\d))/g, separator);
}

/**
 * ************************************** NOTIFICATION GROUP*******************
 */

/**
 * Función genérica para mostrar u ocultar alerta. Si mensaje está en blanco, se
 * oculta.
 */
htroot.showMsg = function(idPrefix, alertId, alertMsgId, msg) {
	if (htroot.isBlank(msg)) {
		$("#" + idPrefix + alertId).hide();
	} else {
		var sanitizedMsg = htroot.htmlSanitize(msg);
		$("#" + idPrefix + alertMsgId).html(sanitizedMsg);
		$("#" + idPrefix + alertId).show();
	}
}

htroot.showErrorMsg = function(idPrefix, msg) {
	htroot.showMsg(idPrefix, "errorAlert", "errorAlertMsg", msg);
}

htroot.showSuccessMsg = function(idPrefix, msg) {
	htroot.showMsg(idPrefix, "successAlert", "successAlertMsg", msg);
}

htroot.showInfoMsg = function(idPrefix, msg) {
	htroot.showMsg(idPrefix, "infoAlert", "infoAlertMsg", msg);
}

htroot.showNotifications = function(idPrefix, infoMsg, successMsg, errorMsg) {
	htroot.showInfoMsg(idPrefix, infoMsg);
	htroot.showSuccessMsg(idPrefix, successMsg);
	htroot.showErrorMsg(idPrefix, errorMsg);
}

/**
 * ********************************** END NOTIFICATION GROUP*******************
 */

/**
 * ************************************** CREATE FIELDS************************
 */

htroot.getInput = function(formId, inputId) {
	var id = inputId.replace(".", "\\.");
	if (htroot.isBlank(formId)) {
		return $('#' + id);
	} else {
		return $('form#' + formId + ' input[id="' + id + '"]');
	}
}

htroot.createNumberField = function(formId, inputId, inputStrId, value, readonly) {
	var $input = htroot.getInput(formId, inputId);
	$input.val(value);
	var $inputStr = htroot.getInput(formId, inputStrId);

	$inputStr.autoNumeric('init', {
		aPad : false,
		lZero: 'keep'
	});

	if (!htroot.isBlank(value)) {
		$inputStr.autoNumeric('set', value);
	}

	$inputStr.blur(function() {
		var val = this.value.replace(/\./g, '').replace(",", ".");
		$input.val(val);
		$input.trigger('change');
	});

	$inputStr.click(function() {
		$(this).select();
	});
}

htroot.setNumberFieldValue = function(formId, inputId, inputStrId, value) {
	var $input = htroot.getInput(formId, inputId);
	var $inputStr = htroot.getInput(formId, inputStrId);
	if (!htroot.isBlank(value)) {
		$inputStr.autoNumeric('set', value);
	} else {
		$inputStr.autoNumeric('set', null);
	}
	$input.val(value);
}

htroot.createDateField = function(formId, inputId, mask, value) {
	var $input = htroot.getInput(formId, inputId);
	$input.inputmask(mask);
	$input.val(value);
	$input.click(function() {
		$(this).select();
	});
}

htroot.createDateTimeField = function(formId, inputId,placeholder, hourFormat, value) {
	var $input = htroot.getInput(formId, inputId);
	$input.inputmask({
		alias:'datetime',
		placeholder: placeholder,
		hourFormat: hourFormat
	});
	$input.val(value);
	$input.click(function() {
		$(this).select();
	});
}


htroot.setDateFieldValue = function(formId, inputId, value) {
	var $input = htroot.getInput(formId, inputId);
	$input.val(value);
}

htroot.createCheckBoxField = function(formId, inputId, checked) {
	var $input = htroot.getInput(formId, inputId);
	$input.iCheck({
		checkboxClass : 'icheckbox_square-blue'
	});
	htroot.setCheckBoxFieldValue(formId, inputId, checked);
}

/** Asigna valor para un input de tipo CheckBox */
htroot.setCheckBoxFieldValue = function(formId, inputId, checked) {
	checked = checked == true || checked == "true"
	var $input = htroot.getInput(formId, inputId);
	$input.prop("checked", checked);
	if (checked) {
		$input.parent().addClass("checked");
	} else {
		$input.parent().removeClass("checked");
	}
}

htroot.createRadioField = function(formId, inputId, checked) {
	var $input = htroot.getInput(formId, inputId);
	$input.iCheck({
		radioClass : 'iradio_square-blue'
	});
	// htroot.setCheckBoxFieldValue(formId, inputId, checked);
}

/** Asigna valor para un input de tipo Radio */
htroot.setRadioFieldValue = function(formId, inputId, value) {
	var $input = htroot.getInput(formId, inputId);
	var checked = $input.val() == value;
	if (checked) {
		$input.parent().addClass("checked");
	} else {
		$input.parent().removeClass("checked");
	}

	$input.prop("checked", checked);
}

/**
 * ********************************** END CREATE FIELDS************************
 */

/**
 * ********************************** DATATABLE********************************
 */
/**
 * @param columnsStr
 *            "id;nombre;apellido"
 * @return {"data":"id", "data":"nombre", "data":"apellido"}
 */
htroot.strToDataTableColumns = function(columnsStr) {

	var arrayCol = columnsStr.split(";");

	var dictColumns = [];

	jQuery.each(arrayCol, function(i, val) {
		dictColumns.push({
			"data" : val
		});
	});
	return dictColumns;
}

htroot.deleteDataTableRecord = function(event, id, formId, url, table) {

	if (formId) {
		$('form#' + formId + ' input[id=id]').val(id);
		$("#" + formId + "deleteBtn").click();
	} else {

	}
	event.stopPropagation();
}

htroot.createDataTableEditButton = function(config, data, type, row, meta) {
	if (!config['showEditButton']) {
		return "";
	}
	$btn = $("<button type='button'  class='btn btn-danger' style='margin-right:2px'><i class='fa fa-edit fa-lg'></i></button>");

	return $btn.prop('outerHTML');
}

htroot.createDataTableDeleteButton = function(config, data, type, row, meta) {
	if (!config['showDeleteButton']) {
		return "";
	}
	var dataTableName = config['name'];

	var $btn = $("<button type='button' onclick='htroot.deleteDataTableRecord(event,"
			+ row.id
			+ ", \""
			+ config["ajaxFormId"]
			+ "\")' class='btn btn-danger'><i class='fa fa-trash-o fa-lg'></i></button>");
	var buttonId = dataTableName + "DeleteBtn" + row.id;
	$btn.attr('id', buttonId);
	return $btn.prop('outerHTML');
}

htroot.dataTableRender = function(data, type, row, meta, name) {
	return isNaN(data) ? data : htroot.numberWithCommas(data);
}

htroot.getDataTableDefaultColumnDefs = function(config) {

	var columnsDefs = [];
	if (config['showActionButtons']) {
		var deleteUrl = config['deleteUrl'];
		var dataTableName = config['name'];
		columnsDefs.push({
			"targets" : -1,
			"data" : null,
			"render" : function(data, type, row, meta) {

				return htroot.createDataTableEditButton(config, data, type,
						row, meta)
						+ htroot.createDataTableDeleteButton(config, data,
								type, row, meta);
			}
		});
	}
	var renderDT = config['dataTableRender'] || htroot.dataTableRender;

	columnsDefs.push({
		"targets" : '_all',
		"render" : function(data, type, row, meta) {
			return renderDT(data, type, row, meta, name);
		}
	});
	return columnsDefs;
}

/**
 * Puede ser que no todas las configuraciones de DataTable se hayan definido,
 * por lo tanto se toman las configuraciones por defecto
 */
htroot.getDataTablaDefaultConfig = function(config) {
	config['processing'] = htroot.coalesce(config['processing'], true);
	config['responsive'] = htroot.coalesce(config['responsive'], true);
	config['stateSave'] = htroot.coalesce(config['stateSave'], true);
	config['bRetrieve'] = htroot.coalesce(config['bRetrieve'], true);
	config['columnDefs'] = htroot.coalesce(config['columnDefs'], htroot
			.getDataTableDefaultColumnDefs(config));
	config['requestParams'] = htroot.coalesce(config['requestParams'], {});
	config['fnSetFilteringDelay'] = htroot.coalesce(
			config['fnSetFilteringDelay'], 1000);
	config['fnServerData'] = htroot.coalesce(config['fnServerData'],
			htroot.fnServerData);
	return config;
}

htroot.getDataTableSortedCols = function(dataTable, editUrl, filter) {
	var sortedCol = "";
	var sortedDir = "";
	for (i = 0; i < dataTable.fnSettings().aaSorting.length; i++) {
		sortedCol = sortedCol + "&iSortCol_" + i + "="
				+ dataTable.fnSettings().aaSorting[i][0];
		sortedDir = sortedDir + "&sSortDir_" + i + "="
				+ dataTable.fnSettings().aaSorting[i][1];
	}
	return sortedCol + sortedDir + "&iSortingCols="
			+ dataTable.fnSettings().aaSorting.length;
}

/** Carga los parámetros de dataTable que serán enviados al servidor */
htroot.addDataTableServerParams = function(aoData, oSettings) {
	for ( var key in oSettings.oInit.requestParams) {
		var value = oSettings.oInit.requestParams[key];
		aoData.push({
			"name" : key,
			"value" : value
		});
	}
	return aoData;
}

/**
 * Función que se ejecuta antes de invocar al servidor y después de obtener
 * respuesta. Se procesa JSON, evita XSS
 */
htroot.fnServerData = function(sSource, aoData, fnCallback, oSettings) {
	oSettings.jqXHR = $.ajax({
		"dataType" : 'json',
		"type" : "GET",
		"url" : sSource,
		"data" : htroot.addDataTableServerParams(aoData, oSettings),
		"success" : function(data) {
			$.each(data.aaData, function(index, item) {
				$.each(item, function(key, value) {
					htroot.objectSanitize(item, key, value);
				});
			});
			fnCallback(data);
		}
	});
}

htroot.getReportUrl = function(editUrl, reportUrl) {
	if (!htroot.isBlank(reportUrl)) {
		/*
		 * Si reporte ya tiene parámetros predefinidos->
		 * /reporte/alumnos?facultad_id=1&anho=2016, entonces se agrega &
		 */
		var addParam = reportUrl.indexOf("?") != -1 ? '&' : '?';
		return reportUrl + addParam + 'sSearch=';
	}
	if (editUrl != null && editUrl != "null") {
		return editUrl + 'getpdf?sSearch=';
	}
	return null;
}

htroot.onPdfButtonClicked = function(dataTable, config, reportUrl) {
	var tableName = config['name'];

	var sortAndParams = htroot.getDataTableSortedCols(dataTable);
	var oSettings = dataTable.fnSettings();
	for ( var key in oSettings.oInit.requestParams) {
		var value = oSettings.oInit.requestParams[key];
		sortAndParams += "&" + key + "=" + value;
	}
	var filter = $('#' + tableName + '_filter input').val();
	var pdfUrl = reportUrl + filter + sortAndParams;
	console.log("Generando PDF:", pdfUrl);
	var woptions = "toolbar=no, scrollbars=yes, resizable=yes, top=10, left=10, width=600, height=800";
	window.open(pdfUrl, "_blank", woptions);
}

htroot.createDataTableAncestor = function(config) {

	config = htroot.getDataTablaDefaultConfig(config);
	var dataTableColumnsStr = config['columnsStr'];
	if (config['showActionButtons']) {
		dataTableColumnsStr = dataTableColumnsStr + ";actionButtons";
	}
	config['columns'] = htroot.strToDataTableColumns(dataTableColumnsStr);

	var dataTableAncestor = $('#' + config['name']).dataTable(config);
	dataTableAncestor.dataTable().fnSetFilteringDelay(
			config['fnSetFilteringDelay']);
	dataTableAncestor.addClass('datatable');
	return dataTableAncestor;
}

htroot.createDataTable = function(config) {
	var dataTable = htroot.createDataTableAncestor(config);
	console.log("DataTable config: {}", config);

	$.each(config['hiddenColumns'], function(index, col) {
		dataTable.fnSetColumnVis(col, false);
	});

	var editUrl = config['editUrl'];
	var reportUrl = config['reportUrl'];
	var name = config['name'];

	if (!htroot.isBlank(editUrl)) {
		$('#' + name + ' tbody').on('click', 'tr', function() {
			var fid = dataTable.fnGetData(this, config['rowIdIndex']);
			if (isNaN(fid)) {
				return;
			}
			htroot.showOverlay();
			var ajaxFormId = config['ajaxFormId'];
			if (ajaxFormId) {
				htroot.findRecord(ajaxFormId, editUrl + "find/" + fid);
			} else {
				window.location.href = editUrl + fid;
			}
		});
	}

	reportUrl = htroot.getReportUrl(editUrl, reportUrl);

	if (!htroot.isBlank(reportUrl)) {
		$('#' + name + '_pdf').click(function() {
			htroot.onPdfButtonClicked(dataTable, config, reportUrl);
		});
	}
	htroot.dataTableMap[config["name"]] = dataTable;
	return dataTable;
}

htroot.refreshDataTable = function(name, params) {
	var table = $("#" + name).dataTable();
	var dictionary = {};
	$.each(params, function(index, item) {
		dictionary[index.valueOf()] = item;
	});
	var oSettings = table.fnSettings();
	oSettings.oInit.requestParams = dictionary;
	table.DataTable().ajax.reload();
}


/**
 * ****************************** END DATATABLE********************************
 */

/**
 * ****************************** MAGIC SUGGEST********************************
 */


htroot.msMaxSelectionRenderer = function() {
	return ''
}

htroot.getMsDefaultConfig = function(config) {

	config['maxSelectionRenderer'] = htroot.coalesce(
			config['maxSelectionRenderer'], htroot.msMaxSelectionRenderer);
	config['placeholder'] = htroot.coalesce(config['placeholder'],
			"Ingrese texto para buscar");
	config['noSuggestionText'] = htroot.coalesce(config['noSuggestionText'],
			"Sin resultados -> {{query}}");
	config['selectFirst'] = htroot.coalesce(config['selectFirst'], true);
	config['autoSelect'] = htroot.coalesce(config['autoSelect'], true);
	config['maxSelection'] = htroot.coalesce(config['maxSelection'], 1);
	config['queryParam'] = htroot.coalesce(config['queryParam'], "sSearch");
	config['method'] = htroot.coalesce(config['method'], "get");
	config['required'] = htroot.coalesce(config['required'], "true");
	return config;
}

htroot.createMagicSuggest = function(config) {
	config = htroot.getMsDefaultConfig(config);
	var $input = null;
	var inputId = config.field;
	var formId = config.formId;
	if (!htroot.isBlank(formId)) {
		$input = $('form#' + formId + " :input[id='" + inputId + "']");
	} else {
		$input = $('#' + inputId);
		formId = '';
	}
	var msId = formId + inputId + '_ms';
	$input.addClass('magicSuggest ' + inputId);
	var $ms = $('<input type="text" id="' + msId
			+ '" class="form-control" />" ');

	var $parent = $input.parent();
	$parent.empty();

	$parent.append($input);
	$parent.append($ms);
	if (config.showFormBtn) {
		htroot.createSuggestOpenFormButton(msId, formId, inputId,
				config.formUrl, config.formWidth, config.formHeight);
	}
	config.data = function(sSearch) {
		var dataEscaped;
		$.ajax({
			"dataType" : 'json',
			"type" : "GET",
			"data" : {
				'sSearch' : sSearch
			},
			"url" : config.source,
			"async" : false,
			"success" : function(data) {
				$.each(data, function(index, item) {
					$.each(item, function(key, value) {
						item[key.valueOf()] = htroot.htmlSanitize(value);
					});
				});
				dataEscaped = data;
			}
		});
		return dataEscaped;
	};

	var suggest = $($ms).magicSuggest(config);

	htroot.suggestMap[msId] = suggest;
	htroot.suggestMap[formId + inputId] = suggest;
	htroot.suggestMap[inputId] = suggest;

	if (!htroot.isBlank(config.idValue)) {
		var data = {};
		data[config.valueField.valueOf()] = config.idValue;
		data[config.displayField.valueOf()] = config.displayValue;
		$input.val(config.idValue);
		suggest.addToSelection([ data ]);
		suggest.setValue(data);
	}

	$(suggest).on('selectionchange', function(e, m) {

		var value = this.getValue();
		$input.val(value);
	});

	return suggest;
}

htroot.createSuggestOpenFormButton = function(msId, formId, inputId, formUrl,
		formWidth, formHeight) {

	var saveListener = function(window, fid, response) {
		if (response.success) {
			htroot.setMagicSuggestValue(formId, inputId, response.data);
			window.close();
		}
	};

	$('#' + formId + inputId.replace('.', '\\.') + 'showForm').click(
			function() {
				htroot.openFormInWindow(formId, formUrl, formWidth, formHeight,
						saveListener);
			});
}

htroot.setMagicSuggestValue = function(formId, inputId, value) {

	if (htroot.isBlank(formId)) {
		formId = '';// en caso de null
	}
	var s1 = htroot.suggestMap[formId + inputId + "_ms"];
	var s2 = htroot.suggestMap[formId + inputId];
	var s3 = htroot.suggestMap[inputId]; 	
	
	var suggest = s1 || s2 || s3;
	if (!suggest) {
		console.log("No se encontró magicSuggest con id:", formId, inputId,
				". Map: ", htroot.suggestMap);
		return;
	}
	suggest.clear();
	if (!htroot.isBlank(value)) {
		var item = value;
		$.each(item, function(key, val) {
			htroot.objectSanitize(item, key, val);
		});
		suggest.addToSelection([ item ]);
		suggest.setValue(item);
	}
}

/**
 * ************************** END MAGIC SUGGEST********************************
 */



/**
 * ************************** AJAX FORM ***************************************
 */

/**
 * Recorre todos los inputs del formulario, utiliza data para asignar valores a
 * campos
 */
htroot.defaultFormLoader = function(formId, response, isNewRecord) {
	var data = response.data;
	$("form#" + formId + " :input[name!='_csrf']").each(
			function() {
				var $input = $(this);
				if ($input.attr("type") != "button") {
					htroot.setFieldValue(formId, $input, $input.attr('name'),
							data, isNewRecord);
				}
			});
	// extraValues
	$.each(response.extraValues, function(key, value) {
		var input = $("#" + key);
		var inputType = $(input).attr("type");
		if (inputType) {
			$(input).val(value);
		} else {
			$(input).html(htroot.htmlSanitize(value));
		}
	});
}

htroot.beforeLoadFormData = function(formId, response, newRecord) {}

htroot.loadFormData = function(formId, response, newRecord) {
	var formLoader = htroot.formLoaderMap[formId];
	htroot.beforeLoadFormData(formId, response, newRecord);
	if (formLoader) {
		htroot.formLoader(formId, response, newRecord);
	} else {
		htroot.defaultFormLoader(formId, response, newRecord);
	}
}

htroot.checkButtonsOnCreateButtonClicked = function(formId) {
	$("#" + formId + "createBtn").hide();
	$("#" + formId + "saveBtn").show();
	$("#" + formId + "deleteBtn").hide();
}

htroot.checkButtonsOnEditButtonClicked = function(formId) {
	$("#" + formId + "createBtn").show();
	$("#" + formId + "saveBtn").show();
	$("#" + formId + "deleteBtn").show();
}

htroot.checkButtonsOnDeleteButtonClicked = function(formId) {
	$("#" + formId + "createBtn").show();
	$("#" + formId + "saveBtn").show();
	$("#" + formId + "deleteBtn").hide();
}


htroot.afterCreateSuccess = function(formId, resp) {
	
}

htroot.afterSaveSuccess = function(formId, resp) {
	
}

htroot.afterFindSuccess = function(formId, resp) {
	
}


htroot.onCreateButtonClicked = function(formId) {
	$('#tabDetalle').tab('show');
	// limpiar panel de errores
	htroot.showValidationErrors(formId, []);

	var url = $("#" + formId).attr('action') + "new";
	var data = {
		"_csrf" : $('form#' + formId + ' input[name=_csrf]').val()
	};
	htroot.showOverlay();
	var jqxhr = $.post(
			url,
			data,
			function(resp) {
				htroot.showNotifications(formId, resp.infoMessage,
						resp.successMessage, resp.errorMessage);
				console.log(resp);
				if (resp.success) {
					htroot.loadFormData(formId, resp, true);
					var input = $('form#' + formId
							+ ' input[autofocus="autofocus"]');
					if (input) {
						$(input).focus();
					}
					htroot.checkButtonsOnCreateButtonClicked(formId);
					$("#" + formId).trigger("change", [ "new", resp ]);
					htroot.afterCreateSuccess(formId, resp);
				}else {
					$("#" + formId).trigger("change", [ "new", resp ]);
				}
				htroot.hideOverlay();
			}).fail(function(jqXHR, textStatus, errorThrown) {
		console.log("Error: ", errorThrown);
		htroot.hideOverlay();
		htroot.showNotifications(formId, null, null, errorThrown);
	});
}

htroot.onSaveButtonClicked = function(formId) {
	var url = $("#" + formId).attr('action') + 'save?_csrf='
			+ $('form#' + formId + ' input[name=_csrf]').val();

	var formData = new FormData($("#" + formId)[0]);

	htroot.showOverlay();

	var jqxhr = $.ajax({
		type : 'POST',
		url : url,
		data : formData,
		processData : false,
		contentType : false,
		complete : function(jqXHR) {
			var resp = jqXHR.responseJSON;
			console.log(resp);
			htroot.showNotifications(formId, resp.infoMessage,
					resp.successMessage, resp.errorMessage);
			htroot.showValidationErrors(formId, resp.fieldErrors);

			if (resp.success) {
				htroot.loadFormData(formId, resp);
				htroot.checkButtonsOnEditButtonClicked(formId);
				htroot.reloadFormDataTable(formId);
				$('form#' + formId + ' input[autofocus="autofocus"]').focus();
				$("#" + formId).trigger("change", [ "save", resp ]);
			    htroot.afterSaveSuccess(formId, resp);
			}else {
			   	$("#" + formId).trigger("change", [ "save", resp ]);
		    }
			if (window.saveListener) {
				window.saveListener(window, formId, resp);
			}
			
			htroot.hideOverlay();
		}
	});
}

htroot.findRecord = function(formId, url) {
	console.log("Buscando registro-> URL:", url, "formId:", formId);
	var jqxhr = $.get(
			url,
			function(resp) {
				console.log(resp);
				htroot.showNotifications(formId, resp.infoMessage,
						resp.successMessage, resp.errorMessage);
				htroot.hideOverlay();
				htroot.showValidationErrors(formId, []);
				if (resp.success) {
					htroot.loadFormData(formId, resp);
					htroot.checkButtonsOnEditButtonClicked(formId);
					$('#tabDetalle').tab('show');
					$("#" + formId).trigger("change", [ "find", resp ]);
					htroot.afterFindSuccess(formId, resp);
				} else {
				    $("#" + formId).trigger("change", [ "find", resp ]);
				}

			}).fail(function(jqXHR, textStatus, errorThrown) {
		htroot.hideOverlay();
		console.log("Error: ", textStatus, errorThrown);
		htroot.showNotifications(formId, null, null, errorThrown);
	});
}

htroot.deleteRecord = function(formId) {
	htroot.showOverlay();
	var recordId = $('form#' + formId + ' input[id=id]').val();
	var url = $("#" + formId).attr('action') + 'destroy-by-id/';
	var data = {
		"_csrf" : $('form#' + formId + ' input[name=_csrf]').val()
	};
	console.log("Borrando registro-> URL:", url, "formId:", formId,
			",recordId:", recordId);
	var jqxhr = $.ajax({
		    type : 'POST',
			url:url + recordId,
			data: data,
			complete : function(jqXHR) {
				var resp = jqXHR.responseJSON;
				console.log(resp);
				htroot.showNotifications(formId, resp.infoMessage,
						resp.successMessage, resp.errorMessage);
				htroot.hideOverlay();
				if (resp.success) {
					// valores cargados en newBeanInstance no van a estar
					resp.data = {};
					htroot.loadFormData(formId, resp);
					htroot.checkButtonsOnDeleteButtonClicked(formId);
					htroot.reloadFormDataTable(formId);
				}
				$("#" + formId).trigger("change", [ "delete", resp ]);

			}});
	
}

htroot.showValidationErrors = function(formId, fieldErrors) {
	if (fieldErrors == null) {
		fieldErrors = [];
	}
	var $ul = $("#" + formId + "fieldErrorsContainer");
	$ul.empty();
	var firstInputWithError = null;

	$("form#" + formId + " :input[name!='_csrf']").each(function() {
		try {
			/* Obtenemos todos los inputs y sus ids */
			var $input = $(this);
			var inputId = $input.attr("id");
			var fieldErrorArray = $.grep(fieldErrors, function(e) {
				return e.field == inputId;
			});

			var $inputDiv = $input.closest('div[class^="form-group"]');
			if (fieldErrorArray.length == 1) {
				fieldError = fieldErrorArray[0];
				$inputDiv.removeClass('has-success');
				$inputDiv.addClass('has-error');
				var $li = $("<li></li>");
				$li.html(htroot.htmlSanitize(fieldError.defaultMessage));
				$ul.append($li);
				if (firstInputWithError == null) {
					firstInputWithError = $input;
				}
			} else {
				$inputDiv.removeClass('has-error');
				$inputDiv.addClass('has-success');
			}

		} catch (err) {
			console.log("Error al cargar formulario:", formId, err);
		}
	});

	if (firstInputWithError != null) {
		$(firstInputWithError).focus();
	}
}

/**
 * Por lo general, un Form está asociado a un datatable. Si se agrega un nuevo
 * registro con el Form, se debe recargar dataTable. Mediante el id del form,
 * intentaremos obtener el datatable asociado
 */
htroot.reloadFormDataTable = function(formId) {
	/**
	 * Ej: FormId: alumnoForm, DataTableId: alumno
	 */
	var dataTableId = formId.substring(0, formId.length - 4);
	console.log("Recargando tabla: ", dataTableId);
	var dataTable = htroot.dataTableMap[dataTableId];
	if (dataTable) {
		dataTable.DataTable().ajax.reload();
	}
}

htroot.checkValueRequired = function(input, value) {
	var $inputDiv = input.closest('div[class^="form-group"]');
	if (htroot.isBlank(value)) {
		value = "";
		if ($inputDiv.hasClass('required')) {
			$inputDiv.addClass('has-error');
		}
	} else {
		if ($inputDiv.hasClass('required')) {
			$inputDiv.removeClass('has-error');
		}
	}
}

/**
 * <code>
 * @param formId: Id del formulario al cual pertenecen los campos.
 * @param input: El input del formulario que será cargado
 * @param dataKey: El campo del objeto (cedula, nombre, apellido)
 * @param data: El objeto del cual se obtendrán los datos. Ej 
 *  data =  {"id":7,"cedula":"123456","nombre":"JUAN PEREZ","institucion":{"id":2,"codigo":"REC","nombre":"RECTORADO"}}
 *     
 *  entonces tendríamos 
 *   <input type="hidden" name="id"/>    -> data['id']
 *   <input type="text" name="cedula"/>  -> data['cedula']
 *    <input type="text" name="nombre"/> -> data['nombre']
 *   <input type="hidden" name="institucion.id"/> -> data['institucion']['id']
 *   
 * </code>
 */
htroot.setFieldValue = function(formId, input, dataKey, data, isNewRecord) {
	// se puede personalizar carga de formulario
	var fieldValueSetter = htroot.fieldValueSetterMap[formId];

	var inputId = input.attr('id');
	if (!dataKey) {
		dataKey = inputId;
	}
	if (!dataKey) {
		return;
	}
	if (fieldValueSetter) {
		fieldValueSetter(formId, input, inputId, data);
		return;
	}
	var value = null;

	$.each(dataKey.split("."), function(key, id) {
		if (key == 0) {
			value = data[id];
		} else if (value) {
			value = value[id];
		}
	});

	htroot.checkValueRequired(input, value);
	var inputType = input.prop("type");
	if (inputType == "checkbox") {
		htroot.setCheckBoxFieldValue(formId, inputId, value);
	} else if (inputType == "radio") {
		htroot.setRadioFieldValue(formId, inputId, value);
	} else if ($(input).hasClass("magicSuggest")) {
		htroot.setMagicSuggestValue(formId, inputId, value);
	} else if ($(input).hasClass("number-input")) {
		htroot.setNumberFieldValue(formId, inputId, inputId + 'Str', value);
	} else if (value != null && value.constructor == Object && value["id"]) {
		/* cuando sea un JsonObject, asumimos que tiene un id */
		input.val(value["id"]);
	} else if (inputType == "select-one" && value == null && isNewRecord) {
		/*
		 * Si es un select y se trata de un nuevo registro, se selecciona la
		 * primera opción
		 */
		input.prop("selectedIndex", 0);
	} else if (inputType == "select-one") {
		input.val(value + "");
	} else {
		input.val(value);
	}
}

/**
 * ************************** END AJAX FORM ***********************************
 */






htroot.createPickerTable = function(pickerId, serverSide, source, columnsStr, fieldId,
		fieldValue, idColumnNumber, valueColumnNumber, lenguaje) {
	/*
	 * - pickerId: el id único asignado al picker. Ej: usuario_picker -
	 * serverSide: si el filtrado de datos se hará con javascript o mediante sql
	 * en el servidor (cada vez que cambie el input filter). Ej: true -
	 * columnsStr: las columnas de la tabla. Ej: "id;codigo;nombre;apellido" -
	 * fieldId: El id del input(generalmente hidden) al que queremos asignar el
	 * id del registro seleccionado. Ej: usuario_id - fieldValue: El id del
	 * input(generalmente tipo text) en el que queremos mostrar el registro
	 * seleccionado. Ej: usuario_nom_ape - idColumnNumber: El número de columna
	 * de la tabla que queremos usar como ID. Si tenemos las columnas ID,
	 * CODIGO, DESCRIPCION, usamos cero puesto que el campo ID queremos guardar -
	 * valueColumnNumber: El número de columna de la tabla cuyo valor queremos
	 * mostrar al usuario. Si tenemos las columnas ID, CODIGO, DESCRIPCION,
	 * usamos 2 puesto que el campo DESCRIPCION queremos mostrar
	 */
	var tableId = pickerId + 'DT';
	var config = {};
	config['name'] = tableId;
	config['sAjaxSource'] = source;
	config['serverSide'] = serverSide;
	config['columnsStr'] = columnsStr;
	config['processing'] = false;
	config['language'] = lenguaje;
	config['showActionButtons'] = false;
	/*
	 * createDataTableAncestor(tableId, serverSide, source, columnsStr,
	 * idColumnNumber, lenguaje);
	 */

	htroot.createDataTableAncestor(config);
	$('#' + tableId + ' tbody').on(
			'click',
			'tr',
			function() {

				var rowId = $('td', this).eq(idColumnNumber).text();
				$('#' + fieldId).val(rowId);
				var columnValue = $('td', this).eq(valueColumnNumber).text();
				$('#' + fieldValue).val(columnValue);
				if (!$(this).hasClass('selected')) {
					$('#' + tableId).DataTable().$('tr.selected').removeClass(
							'selected');
					$(this).addClass('selected');
				}
			});
	$('#' + tableId + ' tbody').on('dblclick', 'tr', function() {
		var rowId = $('td', this).eq(idColumnNumber).text();
		$('#' + fieldId).val(rowId);
		var columnValue = $('td', this).eq(valueColumnNumber).text();
		$('#' + fieldValue).val(columnValue);
		$('#' + pickerId + '_btn').click();
	});

}


htroot.createTableFooterSum = function(tableId, columns) {

	var sums = [];
	for (var i = 0; i < columns.length; i++) {
		sums.push(0);
	}

	var rows = $("#" + tableId).find("tbody > tr");
	$.each(rows, function(key, row) {
		$.each(columns, function(index, col) {
			var sumStr = $(row).find("td:eq(" + col + ")").text().replace(
					/\./g, '');
			var sumInt = parseInt(sumStr);
			if (!isNaN(sumInt)) {
				sums[index] += sumInt;
			}
		});
	});

	var footerRow = $("#" + tableId).find("tfoot > tr");
	$.each(columns, function(index, col) {
		var td = $(footerRow).find("td:eq(" + col + ")");
		$(td).html(htroot.numberWithCommas(sums[index].toString()));
	});
}



function getTimeDiff(horaDesdeStr, horaHastaStr) {

	var desdeParts = horaDesdeStr.split(":");
	var hastaParts = horaHastaStr.split(":");

	var fechaDesde = new Date(2000, 0, 1, desdeParts[0], desdeParts[1]);
	var fechaHasta = new Date(2000, 0, 1, hastaParts[0], hastaParts[1]);

	if (fechaHasta < fechaDesde) {
		fechaHasta.setDate(fechaHasta.getDate() + 1);
	}
	return (fechaHasta - fechaDesde) / (1000 * 60 * 60);
}





/**
 * Función para cargar imágenes de formularios que tengan el campo
 * multipartFile. El formulario debe tener un <div id="multipartFileContainer">
 */
htroot.updateFormImg = function (src, id, paramName) {
	console.log("Buscando imagen:", src, id);
	var name = 'multipartFile';
	if (paramName !== undefined && paramName !== null) {
		name = paramName;
	}

	if (htroot.isBlank(id)) {
		id = -1;
	}
	src = src + id + "?rand=" + Math.random();
	var initialPreview = [ "<img src='" + src
			+ "' class='file-preview-image' >" ];
	$("#multipartFileContainer").empty();
	var $foto = $("<input id='multipartFile' name=" + name
			+ " class='file' type='file' data-preview-file-type='image' />");
	$("#multipartFileContainer").append($foto);

	$foto.fileinput({
		language : 'es',
		allowedFileExtensions : [ 'jpg', 'png', 'gif', 'jpeg' ],
		uploadUrl : '#',
		initialPreview : initialPreview
	});

}
/**
 * Función para asociar una imagen(<div id="multipartFileContainer">) a un
 * formulario.
 * 
 * @param formId
 *            el id del formulario que contiene <div
 *            id="multipartFileContainer">
 * @param inputId
 *            el id del input cuyo valor será utilizado para obtener la imagen
 *            (ej: persona.id)
 * @param la
 *            url de la imagen
 */
htroot.bindImageToForm = function(formId, inputId, src, paramName) {
	htroot.updateFormImg(src, -1, paramName);
	$("#" + formId).change(function(event, action) {
		if (action == "find" || action == "new") {
			var id = $("#" + inputId.replace(".", "\\.")).val();
			htroot.updateFormImg(src, id, paramName);
		}
	});
}


htroot.loadDynamicFormListHelper = function (formId, persistResponseList) {
	if (!persistResponseList) {
		persistResponseList = [];
	}
	console.log("Cargando formulario con lista:", formId, "->",
			persistResponseList);
	var config = htroot.dynamicFormListHelperMap[formId];
	var rowContainerId = config['rowContainerId'];
	// se deben eliminar todas las filas de la tabla

	$("#" + rowContainerId).empty();
	/*
	 * crear N filas, igual a la cantidad de registros que recibimos en
	 * persistResponseList
	 */
	for (i = 0; i < persistResponseList.length; i++) {
		$("#" + config['addRowId']).click();
	}
	/*
	 * No siempre se va a comenzar con index = 0, por lo tanto debemos averiguar
	 * que tiene la segunda fila de la tabla (1ra. fila es defaultRow)
	 */
	var firstRow = $("#" + rowContainerId + '  tr')[0];
	var index = 0;
	if (firstRow) {
		index = $(firstRow).find(".index").val();
	}

	var firstInputWithError = null;

	var $formErrorUl = $("#" + formId).find(".formErrorList");
	$formErrorUl.empty();

	$.each(persistResponseList, function(row, pres) {

		var fieldErrors = pres.fieldErrors;
		if (fieldErrors == null) {
			fieldErrors = [];
		}

		$.each(pres.data, function(field, value) {

			var inputId = field + index;
			$input = $("#" + formId + " :input[id='" + inputId + "']");

			if ($input) {
				if ($input.hasClass("number-input")) {
					htroot.setNumberFieldValue(formId, inputId, field + 'Str' + index, value);
				} else {
					htroot.setFieldValue(formId, $input, field, pres.data);
				}
			}

			var fieldErrorArray = $.grep(fieldErrors, function(e) {
				return e.field == field;
			});

			if (fieldErrorArray.length == 1) {
				fieldError = fieldErrorArray[0];
				$input.parent().removeClass('has-success');
				$input.parent().addClass('has-error');
				$input.attr('title', htroot.htmlSanitize(fieldError.defaultMessage));
				if (firstInputWithError == null) {
					firstInputWithError = $input;
				}
			} else {
				$input.parent().removeClass('has-error');
				$input.parent().addClass('has-success');
				$input.attr('title', '');
			}
		});
		if (pres.errorMessage) {
			var $li = $("<li></li>");
			var msg = htroot.htmlSanitize("Fila " + (row + 1) + ": "
					+ pres.errorMessage)
			$li
					.html(msg);
			$formErrorUl.append($li);
		}
		index++;
	});

	var error = "";
	if (firstInputWithError != null) {
		$(firstInputWithError).focus();
	}
}

htroot.createDynamicFormListHelper = function(config) {
	var clone = $.extend({}, config);
	new DynamicListHelper(clone);

	var formId = config['formId'];
	htroot.dynamicFormListHelperMap[formId] = config;

	var defaultRow = $("#" + formId).find(".defaultRow");
	// detach: igual que remove pero mantiene los eventos
	$(defaultRow).detach();

	htroot.defaultRowMap[formId] = defaultRow;
	if (!config['ignoreFormAjaxSubmit']) {
		htroot.createAjaxFormSubmit(formId);
	}
}

htroot.createAjaxFormSubmit = function(formId) {
	var $form = $("#" + formId);
	$form.submit(function(e) {
		htroot.showOverlay();
		$.ajax(
				{
					type : $form.attr('method'),
					url : $form.attr('action'),
					data : $form.serialize(),
					success : function(persistResponseList) {
						htroot.loadDynamicFormListHelper(formId, persistResponseList);
						$("#" + formId).trigger("change",
								[ "save", persistResponseList ]);
					}
				}).done(function() {
					htroot.hideOverlay();
		});

		e.preventDefault();
	});
}



htroot.openFormInWindow = function(formId, url, width, height, saveListener) {
   
	width = htroot.coalesce(width, 700);
	height = htroot.coalesce(height, 500);
	width = parseInt(width);
	height = parseInt(height);
	var left = (screen.width/2)-(width/2);
	var top = (screen.height/2)-(height/2);
	console.log("Opening window->", "top:", top, "left:",left, "width:", width, "height:", height);
	var win = window.open(url, "_blank",
			"toolbar=no,location=no, directories=no, status=no, menubar=no,copyhistory=no,scrollbars=yes,resizable=yes,width=" + width
					+ ",height=" + height+",top="+top+",left="+left);
	win.saveListener = saveListener;
}

htroot.selectAndForm = function(config){
    
    var saveListener = function(window, fid, response) {
		console.log("On close form:", fid, response);
		if(response.success) {
			$('#'+config.selectId).append($('<option>', {
			    value: response.data.id,
			    text: response.data[config.field]
			}));
			$('#'+config.selectId).val(response.data.id);
		    window.close();
		}
	};
	$('#' + config.formBtnId.replace('.','\\.')).click(function() {
		htroot.openFormInWindow(config.parentFormId, config.formUrl, 700, 500, saveListener);
	});
	
}

htroot.showResponseNotification = function(response, containerId) {
	var container = containerId ? "#" + containerId + " " : "";
	if (response.success) {
		$(container + '#successMessage').html(response.successMessage);
		$(container + '#successDiv').show();
		$(container + '#errorDiv').hide();
	} else {
		$(container + '#errorMessage').html(response.errorMessage);
		$(container + '#errorDiv').show();
		$(container + '#successDiv').hide();
	}
}