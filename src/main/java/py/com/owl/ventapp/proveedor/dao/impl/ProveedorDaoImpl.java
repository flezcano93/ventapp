package py.com.owl.ventapp.proveedor.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.proveedor.dao.ProveedorDao;
import py.com.owl.ventapp.proveedor.domain.Proveedor;

@Repository
@Scope("session")
public class ProveedorDaoImpl extends BaseDaoImpl<Proveedor> implements ProveedorDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
