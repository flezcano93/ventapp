package py.com.owl.ventapp.proveedor.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.dao.TipoProveedorDao;
import py.com.owl.ventapp.proveedor.bc.ProveedorBC;
import py.com.owl.ventapp.proveedor.controllers.list.ProveedorList;
import py.com.owl.ventapp.proveedor.domain.Proveedor;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@Scope("session")
@RequestMapping("/proveedor")
public class ProveedorForm extends BaseForm<Proveedor> {

	@Autowired
	private ProveedorBC proveedorBC;

	@Autowired
	private TipoProveedorDao tipoProveedorDao;

	// vincular formulario con una lista
	@Autowired
	private ProveedorList proveedorList;

	@Override
	public BusinessController<Proveedor> getBusinessController() {
		return proveedorBC;

	}

	@Override
	protected ProveedorList getListController() {

		return proveedorList;

	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/proveedor/proveedor_index";

	}

	@Override
	protected void addExtraAttributes(Proveedor bean, ModelMap modelMap) {

		modelMap.addAttribute("tipoProveedorList", tipoProveedorDao.findEntities());

		super.addExtraAttributes(bean, modelMap);
	}

}
