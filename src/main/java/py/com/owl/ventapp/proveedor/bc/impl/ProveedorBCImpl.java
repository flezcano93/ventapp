package py.com.owl.ventapp.proveedor.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.proveedor.bc.ProveedorBC;
import py.com.owl.ventapp.proveedor.dao.ProveedorDao;
import py.com.owl.ventapp.proveedor.domain.Proveedor;
import py.una.cnc.htroot.core.dao.Dao;

@Scope("session")
@Component
public class ProveedorBCImpl extends BaseBCImpl<Proveedor> implements ProveedorBC {

	@Autowired
	private ProveedorDao proveedorDao; // = new TipoClienteDaoImpl ();

	@Override
	public Dao<Proveedor> getDAOInstance() {
		return proveedorDao;
	}

}
