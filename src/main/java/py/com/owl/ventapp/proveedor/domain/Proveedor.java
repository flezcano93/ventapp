package py.com.owl.ventapp.proveedor.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.TipoProveedor;
import py.una.cnc.htroot.core.domain.EntityWithImage;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "proveedor_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "proveedor_ruc_uk", columnNames = { "empresa_id", "ruc" }) })

public class Proveedor extends GenericEntity implements EntityWithImage {

	private static final String SECUENCIA = "proveedor_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{proveedor.codigo.notNull}")
	@NotBlank(message = "{proveedor.codigo.notBlank}")
	@Size(max = 10, message = "{proveedor.codigo.size}")
	private String codigo;

	@NotNull(message = "{proveedor.ruc.notNull}")
	@NotBlank(message = "{proveedor.ruc.notBlank}")
	@Size(max = 10, message = "{proveedor.ruc.size}")
	private String ruc;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "proveedor_empresa_fk"))
	@NotNull(message = "{proveedor.empresa.NotNull}")
	private Empresa empresa;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "proveedor_tipoproveedor_fk"))
	@NotNull(message = "{proveedor.tipoproveedor.NotNull}")
	private TipoProveedor tipoProveedor;

	@Email(message = "{email.invalido}")
	@Size(max = 100, message = "{proveedor.email.size}")
	private String email;

	@NotNull(message = "{proveedor.nombre.notNull}")
	@NotBlank(message = "{proveedor.nombre.notBlank}")
	@Size(max = 10, message = "{proveedor.nombre.size}")
	private String nombre;

	@NotNull(message = "{proveedor.apellido.notNull}")
	@NotBlank(message = "{proveedor.apellido.notBlank}")
	@Size(max = 10, message = "{proveedor.apellido.size}")
	private String apellido;

	@JsonIgnore
	@Lob
	@Basic(fetch = FetchType.LAZY, optional = true)
	private byte[] image;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public TipoProveedor getTipoProveedor() {
		return tipoProveedor;
	}

	public void setTipoProveedor(TipoProveedor tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public byte[] getImage() {
		return image;
	}

	@Override
	public void setImage(byte[] image) {
		this.image = image;
	}

	private transient MultipartFile multipartFile;

	@Override
	public void setMultipartFile(MultipartFile multipartFile) {
		// TODO Auto-generated method stub
		this.multipartFile = multipartFile;
	}

	@Override
	public MultipartFile getMultipartFile() {
		// TODO Auto-generated method stub
		return multipartFile;
	}

	public Proveedor() {
		empresa = new Empresa();
	}

}
