package py.com.owl.ventapp.proveedor.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.proveedor.domain.Proveedor;

public interface ProveedorBC extends BaseBC<Proveedor> {

}