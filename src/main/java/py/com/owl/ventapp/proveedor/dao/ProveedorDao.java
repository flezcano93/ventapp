package py.com.owl.ventapp.proveedor.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.proveedor.domain.Proveedor;

public interface ProveedorDao extends BaseDao<Proveedor> {

}