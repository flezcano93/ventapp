package py.com.owl.ventapp.proveedor.controllers.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.proveedor.bc.ProveedorBC;
import py.com.owl.ventapp.proveedor.domain.Proveedor;

@Controller
@Scope("session")
@RequestMapping("/proveedor")
public class ProveedorList extends BaseList<Proveedor> {

	@Autowired
	private ProveedorBC proveedorBC; // =new TipoClienteImpl;

	@Override
	public ProveedorBC getBusinessController() {
		// TODO Auto-generated method stub
		return proveedorBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "codigo", "nombre", "apellido", "tipoProveedor.nombre" };

	}

	@Override
	protected String getFilterableColumns() {
		return "codigo||nombre||apellido||tipoProveedor.nombre";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<Proveedor> getJson() {
		return proveedorBC.findEntities();

	}

}
