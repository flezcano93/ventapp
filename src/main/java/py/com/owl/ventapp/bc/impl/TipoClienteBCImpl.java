package py.com.owl.ventapp.bc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.com.owl.ventapp.domain.TipoCliente;
import py.una.cnc.htroot.core.dao.Dao;
import py.una.cnc.lib.db.dataprovider.DataTableModel;

@Scope("session")
@Component
public class TipoClienteBCImpl extends BaseBCImpl<TipoCliente> implements TipoClienteBC{

	@Autowired
	private TipoClienteDao tipoClienteDao; //= new TipoClienteDaoImpl ();
	
	@Override
	public Dao<TipoCliente> getDAOInstance() {
		return tipoClienteDao;
	}
}
	