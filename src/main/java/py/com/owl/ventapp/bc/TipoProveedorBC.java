package py.com.owl.ventapp.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.domain.TipoProveedor;

public interface TipoProveedorBC extends BaseBC<TipoProveedor> {

}
