package py.com.owl.ventapp.bc.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.SucursalBC;
import py.com.owl.ventapp.dao.SucursalDao;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SucursalBCImpl extends BaseBCImpl<Sucursal> implements SucursalBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SucursalDao sucursalDao;

	@Override
	public SucursalDao getDAOInstance() {

		return sucursalDao;
	}

	@Override
	public List<Sucursal> getListByEmpresa(Empresa empresa) {

		return sucursalDao.getListByEmpresa(empresa);
	}

}
