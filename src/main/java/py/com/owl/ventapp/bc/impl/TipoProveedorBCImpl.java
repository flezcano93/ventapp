package py.com.owl.ventapp.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.TipoProveedorBC;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.com.owl.ventapp.dao.TipoProveedorDao;
import py.com.owl.ventapp.domain.TipoCliente;
import py.com.owl.ventapp.domain.TipoProveedor;
import py.una.cnc.htroot.core.dao.Dao;

@Scope("session")
@Component
public class TipoProveedorBCImpl extends BaseBCImpl<TipoProveedor> implements TipoProveedorBC {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private TipoProveedorDao tipoProveedorDao; //= new TipoProveedorDaoImpl ();
	

	@Override
	public Dao<TipoProveedor> getDAOInstance() {
		return tipoProveedorDao;
	}
}
