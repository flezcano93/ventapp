package py.com.owl.ventapp.controllers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.domain.TipoCliente;
import py.com.owl.ventapp.domain.TipoProveedor;
import py.com.owl.ventapp.proveedor.domain.Proveedor;

@RestController
@SessionScope
public class MultiService {

	@Autowired
	private EntityManager em;

	@GetMapping("/json/list/cliente")
	@SuppressWarnings("unchecked")
	public List<Cliente> getClienteList() {

		String sql = "SELECT object(C) FROM Cliente AS C";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

	@GetMapping("/json/list/proveedor")
	@SuppressWarnings("unchecked")
	public List<Proveedor> getProveedorList() {

		String sql = "SELECT object(P) FROM Proveedor AS P";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

	@GetMapping("/json/list/tipo-cliente")
	@SuppressWarnings("unchecked")
	public List<TipoCliente> getTipoClienteList() {

		String sql = "SELECT object(TC) FROM TipoCliente AS TC";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

	@GetMapping("/json/list/tipo-proveedor")
	@SuppressWarnings("unchecked")
	public List<TipoProveedor> getTipoProveedorList() {

		String sql = "SELECT object(TP) FROM TipoProveedor AS TP";
		Query query = em.createQuery(sql);
		return query.getResultList();

	}

}
