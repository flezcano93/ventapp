package py.com.owl.ventapp.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.TipoProveedorBC;
import py.com.owl.ventapp.controllers.lists.TipoProveedorList;
import py.com.owl.ventapp.domain.TipoProveedor;

/*si no le pongo scope se activa Singleton*/
//se le agrega un ambito, el scope
@Controller
@RequestMapping("/tipo-proveedor")
@Scope("session")
// Singleton, Session, Request
public class TipoProveedorForm extends BaseForm<TipoProveedor> {

	@Autowired
	private TipoProveedorBC tipoProveedorBC;

	// vincular formulario con una lista
	@Autowired
	private TipoProveedorList tipoProveedorList;

	@Override
	public TipoProveedorBC getBusinessController() {

		return tipoProveedorBC;
	}

	@Override
	protected TipoProveedorList getListController() {

		return tipoProveedorList;

	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/tipo_proveedor/tipo_proveedor_index";

	}

}
