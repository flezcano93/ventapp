package py.com.owl.ventapp.controllers.lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.bc.TipoProveedorBC;
import py.com.owl.ventapp.domain.TipoCliente;
import py.com.owl.ventapp.domain.TipoProveedor;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@Scope("session")
@RequestMapping("/tipo-proveedor")

public class TipoProveedorList extends BaseList<TipoProveedor> {

	@Autowired
	private TipoProveedorBC tipoProveedorBC;
	
	@Override
	public TipoProveedorBC getBusinessController() {
		// TODO Auto-generated method stub
		return tipoProveedorBC;
	}
	
	@Override
	public String[] getTableColumns(){
		return new String[] {"id","codigo","nombre"};
		
	}
	
	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<TipoProveedor> getJson() {
		return tipoProveedorBC.findEntities();
		
	}
}
