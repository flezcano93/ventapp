package py.com.owl.ventapp.controllers.lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.domain.TipoCliente;

@Controller
@Scope("session")
@RequestMapping("/tipo-cliente")

public class TipoClienteList extends BaseList <TipoCliente>{

	@Autowired
	private TipoClienteBC tipoClienteBC; //=new TipoClienteImpl;
	
	
	@Override
	public TipoClienteBC getBusinessController() {
		// TODO Auto-generated method stub
		return tipoClienteBC;
	}
	
	@Override
	public String[] getTableColumns(){
		return new String[] {"id","codigo","nombre"};
		
	}
	
	
	@Override
	protected String getFilterableColumns(){
		return "codigo||nombre";
	}
	
	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<TipoCliente> getJson() {
		return tipoClienteBC.findEntities();
		
	}
}

