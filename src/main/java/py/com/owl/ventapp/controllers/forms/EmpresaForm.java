package py.com.owl.ventapp.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.EmpresaBC;
import py.com.owl.ventapp.controllers.lists.EmpresaList;
import py.com.owl.ventapp.domain.Empresa;

@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
@RequestMapping(value = "/empresa")
public class EmpresaForm extends BaseForm<Empresa> {

	@Autowired
	private EmpresaBC empresaBC;
	@Autowired
	private EmpresaList empresaListControlador;

	@Override
	public EmpresaBC getBusinessController() {

		return empresaBC;
	}

	@Override
	public String getTemplatePath(ModelMap map) {

		return "abm/empresa/empresa_index";
	}

	@Override
	protected EmpresaList getListController() {

		return empresaListControlador;
	}
}
