package py.com.owl.ventapp.main;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.domain.Empresa;

@Component
@Scope("session")
public class AppSession {

	/* de forma estatica */
	public Empresa getEmpresa() {
		Empresa emp = new Empresa();
		emp.setId(1L);
		return emp;
	}

	public Cliente getCliente() {
		Cliente cli = new Cliente();
		cli.setId(1L);
		return cli;
	}

}
