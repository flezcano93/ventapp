package py.com.owl.ventapp.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.TipoProveedorDao;
import py.com.owl.ventapp.domain.TipoProveedor;

@Repository
@Scope("session")
public class TipoProveedorDaoImpl extends BaseDaoImpl<TipoProveedor> implements TipoProveedorDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
