package py.com.owl.ventapp.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.domain.UnidadMedida;

public interface UnidadMedidaDao extends BaseDao<UnidadMedida> {

}
