package py.com.owl.ventapp.dao;

import java.util.List;
import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

public interface SucursalDao extends BaseDao<Sucursal> {

	List<Sucursal> getListByEmpresa(Empresa empresa);
}
