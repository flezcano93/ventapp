package py.com.owl.ventapp.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.domain.Empresa;

public interface EmpresaDao extends BaseDao<Empresa> {

}
