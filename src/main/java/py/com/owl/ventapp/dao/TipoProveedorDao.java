package py.com.owl.ventapp.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.domain.TipoProveedor;

public interface TipoProveedorDao extends BaseDao<TipoProveedor> {

}
