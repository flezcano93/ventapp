package py.com.owl.ventapp.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.com.owl.ventapp.domain.TipoCliente;
import py.una.cnc.lib.db.dataprovider.DataTableModel;


@Repository
@Scope("session")
public class TipoClienteDaoImpl extends BaseDaoImpl<TipoCliente> implements TipoClienteDao{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	}
