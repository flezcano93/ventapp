package py.com.owl.ventapp.base;

import py.una.cnc.htroot.core.dao.Dao;

public interface BaseDao<T extends GenericEntity> extends Dao<T> {

}
