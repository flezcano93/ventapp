package py.com.owl.ventapp.converters;

import org.springframework.stereotype.Component;

import py.com.owl.ventapp.producto.dao.SubProductoDao;
import py.com.owl.ventapp.producto.domain.SubProducto;
import py.una.cnc.htroot.converters.ModelConverter;
import py.una.cnc.htroot.core.main.ApplicationContextProvider;

@Component
public class SubProductoConverter extends ModelConverter<SubProducto> {

	@Override
	protected SubProductoDao getDaoImpl() {

		return (SubProductoDao) ApplicationContextProvider.getBeanStatic("subProductoDaoImpl");
	}
}
