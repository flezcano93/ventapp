package py.com.owl.ventapp.cliente.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.controllers.lists.ClienteList;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@Scope("session")
@RequestMapping("/cliente")
public class ClienteForm extends BaseForm<Cliente> {

	@Autowired
	private ClienteBC clienteBC;

	@Autowired
	private TipoClienteDao tipoClienteDao;

	// vincular formulario con una lista
	@Autowired
	private ClienteList clienteList;

	@Override
	public BusinessController<Cliente> getBusinessController() {
		return clienteBC;

	}

	@Override
	protected ClienteList getListController() {

		return clienteList;

	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/cliente/cliente_index";

	}

	@Override
	protected void addExtraAttributes(Cliente bean, ModelMap modelMap) {

		modelMap.addAttribute("tipoClienteList", tipoClienteDao.findEntities());

		super.addExtraAttributes(bean, modelMap);
	}

}
