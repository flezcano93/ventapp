package py.com.owl.ventapp.cliente.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.cliente.domain.Cliente;

public interface ClienteDao extends BaseDao<Cliente> {

}
