package py.com.owl.ventapp.cliente.controllers.lists;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@Scope("session")
@RequestMapping("/cliente")
public class ClienteList extends BaseList<Cliente> {

	@Autowired
	private ClienteBC clienteBC; // =new TipoClienteImpl;

	@Override
	public ClienteBC getBusinessController() {
		// TODO Auto-generated method stub
		return clienteBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "codigo", "nombre", "apellido", "tipoCliente.nombre" };

	}

	@Override
	protected String getFilterableColumns() {
		return "codigo||nombre||apellido||tipoCliente.nombre";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<Cliente> getJson() {
		return clienteBC.findEntities();

	}

	/* HorizontalAlign para indicar hacia donde quiero la columna */
	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = (new ReportColumn("codigo", 70, HorizontalAlign.CENTER));
		columnas.add(cod);
		columnas.add(new ReportColumn("nombre", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("apellido", 100, HorizontalAlign.LEFT));
		columnas.add(new ReportColumn("tipoCliente.nombre", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("fechaNacimiento", 100, "dd/MM/yyyy"));
		columnas.add(new ReportColumn("email", 100));
		return columnas;
	}

	@Override
	protected Page getPage() {
		// return super.getPage();
		return Page.Page_A4_Landscape();
	}

}
