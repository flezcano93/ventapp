package py.com.owl.ventapp.venta.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibm.icu.math.BigDecimal;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;
import py.una.cnc.htroot.core.util.PersistResponse;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "ventacab_timbrado_codigo_uk", columnNames = { "empresa_id",
		"timbrado_id", "numdoc" }) })

public class VentaCab extends GenericEntity {

	private static final String SECUENCIA = "ventacab_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	/* no me crea en mi tabla con el transient */
	private transient List<VentaDet> ventaDetList;

	/* el data de persisresponse es VentaDet */
	private transient List<PersistResponse<VentaDet>> ventaDetListResponse;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaCab_timbrado_fk"))
	@NotNull(message = "{ventaCab.timbrado.notNull}")
	private Timbrado timbrado;

	@NotNull(message = "{ventaCab.numdoc.notNull}")
	private Integer numdoc;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaCab_cliente_fk"))
	@NotNull(message = "{ventaCab.cliente.notNull}")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaCab_sucursal_fk"))
	@NotNull(message = "{ventaCab.sucursal.notNull}")
	private Sucursal sucursal;

	private BigDecimal montoTotal = BigDecimal.ZERO;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@NotNull(message = "{timbrado.fechaVenta.notNull}")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaVenta;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "cliente_empresa_fk"))
	@NotNull(message = "{cliente.empresa.notNull}")
	private Empresa empresa;

	public VentaCab() {
		// pasar validacion @notNull
		empresa = new Empresa();
		fechaVenta = new Date();
		ventaDetList = new ArrayList<>();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Timbrado getTimbrado() {
		return timbrado;
	}

	public void setTimbrado(Timbrado timbrado) {
		this.timbrado = timbrado;
	}

	public Integer getNumdoc() {
		return numdoc;
	}

	public void setNumdoc(Integer numdoc) {
		this.numdoc = numdoc;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		// TODO Auto-generated method stub
		this.empresa = empresa;
	}

	public List<VentaDet> getVentaDetList() {
		return ventaDetList;
	}

	public void setVentaDetList(List<VentaDet> ventaDetList) {
		this.ventaDetList = ventaDetList;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public List<PersistResponse<VentaDet>> getVentaDetListResponse() {
		return ventaDetListResponse;
	}

	public void setVentaDetListResponse(List<PersistResponse<VentaDet>> ventaDetListResponse) {
		this.ventaDetListResponse = ventaDetListResponse;
	}

	@Override
	public String toString() {
		return "VentaCab [id=" + id + ", numdoc=" + numdoc + ", cliente=" + cliente + "]";
	}

}
