package py.com.owl.ventapp.venta.controllers.forms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.producto.domain.Iva;
import py.com.owl.ventapp.producto.domain.SubProducto;
import py.com.owl.ventapp.producto.service.IvaService;
import py.com.owl.ventapp.producto.service.SubProductoService;
import py.com.owl.ventapp.venta.bc.VentaDetBC;
import py.com.owl.ventapp.venta.controllers.lists.VentaDetList;
import py.com.owl.ventapp.venta.domain.VentaDet;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@SessionScope
@RequestMapping("/ventadet")
public class VentaDetForm extends BaseForm<VentaDet> {

	@Autowired
	private VentaDetBC ventaDetBC;

	@Autowired
	private VentaDetList ventaDetList;

	@Autowired
	private IvaService ivaService;

	@Autowired
	private SubProductoService subproductoService;

	@Override
	public BusinessController<VentaDet> getBusinessController() {
		// TODO Auto-generated method stub
		return ventaDetBC;
	}

	@Override
	protected VentaDetList getListController() {
		return ventaDetList;
	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/venta/venta_index";

	}

	/* listado de variables */
	@ModelAttribute("subproductoList")
	public List<SubProducto> y() {
		return subproductoService.findEntities();
	}

	/* listado de variables */
	@ModelAttribute("ivaList")
	public List<Iva> x() {
		return ivaService.findEntities();
	}
}
