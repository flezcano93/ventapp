package py.com.owl.ventapp.venta.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.venta.domain.VentaCab;

public interface VentaCabBC extends BaseBC<VentaCab> {

}
