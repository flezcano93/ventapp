package py.com.owl.ventapp.venta.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.venta.dao.VentaCabDao;
import py.com.owl.ventapp.venta.domain.VentaCab;

@Repository
@Scope("session")
public class VentaCabDaoImpl extends BaseDaoImpl<VentaCab> implements VentaCabDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
