package py.com.owl.ventapp.venta.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.venta.bc.TimbradoBC;
import py.com.owl.ventapp.venta.dao.TimbradoDao;
import py.com.owl.ventapp.venta.domain.Timbrado;
import py.una.cnc.htroot.core.dao.Dao;

@Service
@SessionScope
public class TimbradoBCImpl extends BaseBCImpl<Timbrado> implements TimbradoBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private TimbradoDao timbradoDao;

	@Override
	public Dao<Timbrado> getDAOInstance() {
		// TODO Auto-generated method stub
		return timbradoDao;
	}

}
