package py.com.owl.ventapp.venta.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.venta.dao.VentaDetDao;
import py.com.owl.ventapp.venta.domain.VentaCab;
import py.com.owl.ventapp.venta.domain.VentaDet;

@Repository
@SessionScope
public class VentaDetDaoImpl extends BaseDaoImpl<VentaDet> implements VentaDetDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public List<VentaDet> findByVentaCab(VentaCab ventaCab) {

		return findEntitiesByCondition("WHERE ventaCab_id = ?1", ventaCab.getId());
	}

}
