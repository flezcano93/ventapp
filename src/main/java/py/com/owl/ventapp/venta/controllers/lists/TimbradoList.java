package py.com.owl.ventapp.venta.controllers.lists;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.venta.bc.TimbradoBC;
import py.com.owl.ventapp.venta.domain.Timbrado;
import py.una.cnc.htroot.core.bc.BusinessController;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@Scope("session")
@RequestMapping("/timbrado")
public class TimbradoList extends BaseList<Timbrado> {

	@Autowired
	private TimbradoBC timbradoBC;

	@Override
	public BusinessController<Timbrado> getBusinessController() {
		// TODO Auto-generated method stub
		return timbradoBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "numero", "numeroDesde", "numeroHasta", "numeroActual", "fechaDesde",
				"fechaHasta" };
	}

	@Override
	protected String getFilterableColumns() {
		return "numeroActual||fechaDesde";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<Timbrado> getJson() {
		return timbradoBC.findEntities();

	}

	/* HorizontalAlign para indicar hacia donde quiero la columna */
	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = (new ReportColumn("numero", 70, HorizontalAlign.CENTER));
		columnas.add(cod);
		columnas.add(new ReportColumn("numeroDesde", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("numeroHasta", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("numeroActual", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("fechaDesde", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("fechaHasta", 100, HorizontalAlign.RIGHT));
		return columnas;
	}

}
