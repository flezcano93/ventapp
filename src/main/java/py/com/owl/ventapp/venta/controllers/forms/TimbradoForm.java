package py.com.owl.ventapp.venta.controllers.forms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.SucursalBC;
import py.com.owl.ventapp.domain.Sucursal;
import py.com.owl.ventapp.venta.bc.TimbradoBC;
import py.com.owl.ventapp.venta.controllers.lists.TimbradoList;
import py.com.owl.ventapp.venta.domain.Timbrado;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@SessionScope
@RequestMapping("/timbrado")
public class TimbradoForm extends BaseForm<Timbrado> {

	/* controller siempre se conecta con la capa de logica de negocio (BC) */

	@Autowired
	private TimbradoBC timbradoBC;

	@Autowired
	private TimbradoList timbradoList;

	@Autowired
	private SucursalBC sucursalBC;

	@Override
	public BusinessController<Timbrado> getBusinessController() {
		// TODO Auto-generated method stub
		return timbradoBC;
	}

	@Override
	protected TimbradoList getListController() {
		return timbradoList;
	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/timbrado/timbrado_index";

	}

	@ModelAttribute("timbradoList")
	public List<Timbrado> x() {
		return timbradoBC.findEntities();
	}

	@ModelAttribute("sucursalList")
	public List<Sucursal> y() {
		return sucursalBC.findEntities();
	}

}
