package py.com.owl.ventapp.venta.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.venta.bc.VentaDetBC;
import py.com.owl.ventapp.venta.dao.VentaDetDao;
import py.com.owl.ventapp.venta.domain.VentaDet;
import py.una.cnc.htroot.core.dao.Dao;

public class VentaDetBCImpl extends BaseBCImpl<VentaDet> implements VentaDetBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private VentaDetDao ventaDetDao;

	@Override
	public Dao<VentaDet> getDAOInstance() {
		return ventaDetDao;
	}

}
