package py.com.owl.ventapp.venta.bc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.venta.bc.VentaCabBC;
import py.com.owl.ventapp.venta.dao.VentaCabDao;
import py.com.owl.ventapp.venta.dao.VentaDetDao;
import py.com.owl.ventapp.venta.domain.VentaCab;
import py.com.owl.ventapp.venta.domain.VentaDet;
import py.una.cnc.htroot.core.dao.Dao;
import py.una.cnc.htroot.core.exceptions.BusinessLogicException;
import py.una.cnc.htroot.core.util.PersistResponse;
import py.una.cnc.htroot.core.util.PersistResponseUtil;
import py.una.cnc.lib.core.util.AppLogger;

@Service
@SessionScope
public class VentaCabBCImpl extends BaseBCImpl<VentaCab> implements VentaCabBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private VentaCabDao ventaCabDao;

	@Autowired
	private PersistResponseUtil persistResponseUtil;

	AppLogger logger = new AppLogger(getClass());

	@Autowired
	private VentaDetDao ventaDetDao;

	@Override
	public Dao<VentaCab> getDAOInstance() {
		return ventaCabDao;
	}

	@Override
	public void create(VentaCab obj) {
		List<VentaDet> detalles = obj.getVentaDetList();
		cargarDatos(obj);
		List<PersistResponse<VentaDet>> perlist = persistResponseUtil.getPersistResponseList(VentaDet.class, detalles,
				true);
		obj.setVentaDetListResponse(perlist);
		checkValidationErrors(perlist);
		super.create(obj);
		for (VentaDet detail : detalles) {
			ventaDetDao.create(detail);
		}
	}

	private void checkValidationErrors(List<PersistResponse<VentaDet>> perlist) {
		/* si al menos un detalle tiene error de validacion no continuar */
		if (persistResponseUtil.hasError(VentaDet.class, perlist)) {
			for (PersistResponse<VentaDet> pr : perlist) {
				logger.error("FieldErrors{}", pr.getFieldErrors());
			}

			throw new BusinessLogicException("Errores de validacion");
		}
	}

	@Transactional
	@Override
	public VentaCab find(Long id) {
		VentaCab ventaCab = super.find(id);
		if (ventaCab != null) {
			List<VentaDet> details = ventaDetDao.findByVentaCab(ventaCab);

			List<PersistResponse<VentaDet>> perlist = persistResponseUtil.getPersistResponseList(VentaDet.class,
					details, false);
			ventaCab.setVentaDetListResponse(perlist);
		}

		return ventaCab;
	}

	public void cargarDatos(VentaCab ventacab) {
		for (VentaDet detalle : ventacab.getVentaDetList()) {
			detalle.setVentaCab(ventacab);
			logger.info("error{}", detalle.getSubproducto());
			detalle.setIva(detalle.getSubproducto().getProducto().getIva());
			detalle.setPrecio(detalle.getSubproducto().getPrecio());

		}
	}

	@Transactional
	@Override
	public void edit(VentaCab ventaCab) {
		cargarDatos(ventaCab);
		List<PersistResponse<VentaDet>> perlist = persistResponseUtil.getPersistResponseList(VentaDet.class,
				ventaCab.getVentaDetList(), true);
		ventaCab.setVentaDetListResponse(perlist);
		checkValidationErrors(perlist);

		destroyDetalles(ventaCab);
		for (VentaDet detail : ventaCab.getVentaDetList()) {
			ventaDetDao.create(detail);
		}

		super.edit(ventaCab);

	}

	private void destroyDetalles(VentaCab ventaCab) {
		List<VentaDet> details = ventaDetDao.findByVentaCab(ventaCab);
		for (VentaDet detail : details) {
			ventaDetDao.destroy(detail);
		}
	}

	@Transactional
	@Override
	public void destroy(VentaCab ventaCab) {

		destroyDetalles(ventaCab);
		super.destroy(ventaCab);
	}

}
