package py.com.owl.ventapp.venta.controllers.forms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.SucursalBC;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.domain.Sucursal;
import py.com.owl.ventapp.producto.domain.SubProducto;
import py.com.owl.ventapp.producto.service.ProductoService;
import py.com.owl.ventapp.producto.service.SubProductoService;
import py.com.owl.ventapp.venta.bc.TimbradoBC;
import py.com.owl.ventapp.venta.bc.VentaCabBC;
import py.com.owl.ventapp.venta.controllers.lists.VentaCabList;
import py.com.owl.ventapp.venta.domain.Timbrado;
import py.com.owl.ventapp.venta.domain.VentaCab;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@SessionScope
@RequestMapping("/venta")
public class VentaCabForm extends BaseForm<VentaCab> {

	/* controller siempre se conecta con la capa de logica de negocio (BC) */

	@Autowired
	private VentaCabBC ventaCabBC;

	@Autowired
	private VentaCabList ventaList;

	@Autowired
	private ClienteBC clienteBC;

	@Autowired
	private ProductoService productoBC;

	@Autowired
	private SubProductoService subproductoBC;

	@Autowired
	private SucursalBC sucursalBC;

	@Autowired
	private TimbradoBC timbradoBC;

	@Override
	public BusinessController<VentaCab> getBusinessController() {
		return ventaCabBC;
	}

	@Override
	protected VentaCabList getListController() {

		return ventaList;

	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/venta/venta_index";

	}

	/* para listar, tiene que coincidir con el th:each en el menuform */
	@ModelAttribute("timbradoList")
	public List<Timbrado> x() {
		return timbradoBC.findEntities();
	}

	/* listado de variables */
	@ModelAttribute("clienteList")
	public List<Cliente> y() {
		return clienteBC.findEntities();
	}

	/* listado de variables */
	@ModelAttribute("sucursalList")
	public List<Sucursal> z() {
		return sucursalBC.findEntities();
	}

	/* listado de variables */
	@ModelAttribute("productoList")
	public List<SubProducto> p() {
		return subproductoBC.findEntities();
	}

	@GetMapping("/timbrado")
	@ResponseBody
	public Timbrado getTimbradoById(@RequestParam Long timbradoId) {
		return timbradoBC.find(timbradoId);

	}
}
