package py.com.owl.ventapp.venta.dao;

import java.util.List;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.venta.domain.VentaCab;
import py.com.owl.ventapp.venta.domain.VentaDet;

public interface VentaDetDao extends BaseDao<VentaDet> {

	List<VentaDet> findByVentaCab(VentaCab ventaCab);

}
