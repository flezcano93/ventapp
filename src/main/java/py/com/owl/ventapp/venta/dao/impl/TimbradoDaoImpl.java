package py.com.owl.ventapp.venta.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.venta.dao.TimbradoDao;
import py.com.owl.ventapp.venta.domain.Timbrado;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class TimbradoDaoImpl extends BaseDaoImpl<Timbrado> implements TimbradoDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
