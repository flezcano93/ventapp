package py.com.owl.ventapp.venta.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "timbrado_numero_uk", columnNames = { "empresa_id", "numero" }) })

public class Timbrado extends GenericEntity {

	// para generar id sin que el usuario cargue
	private static final String SECUENCIA = "timbrado_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{timbrado.numero.notNull}")
	@Min(value = 1, message = "{timbrado.numero.min}")
	private Integer numero;

	@NotNull(message = "{timbrado.numeroDesde.notNull}")
	@Min(value = 1, message = "{timbrado.numeroDesde.min}")
	private Integer numeroDesde;

	@NotNull(message = "{timbrado.numeroHasta.notNull}")
	@Min(value = 1, message = "{timbrado.numeroHasta.min}")
	private Integer numeroHasta;

	@NotNull(message = "{timbrado.numeroActual.notNull}")
	@Min(value = 1, message = "{timbrado.numeroActual.min}")
	private Integer numeroActual;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "{timbrado.fechaDesde.notNull}")
	@Temporal(TemporalType.DATE)
	private Date fechaDesde;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "{timbrado.fechaHasta.notNull}")
	@Temporal(TemporalType.DATE)
	private Date fechaHasta;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "sucursal_empresa_fk"))
	@NotNull(message = "{sucursal.empresa.notNull}")
	private Sucursal sucursal;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "timbrado_empresa_fk"))
	@NotNull(message = "{timbrado.empresa.notNull}")
	private Empresa empresa;

	public Timbrado() {
		empresa = new Empresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getNumeroDesde() {
		return numeroDesde;
	}

	public void setNumeroDesde(Integer numeroDesde) {
		this.numeroDesde = numeroDesde;
	}

	public Integer getNumeroHasta() {
		return numeroHasta;
	}

	public void setNumeroHasta(Integer numeroHasta) {
		this.numeroHasta = numeroHasta;
	}

	public Integer getNumeroActual() {
		return numeroActual;
	}

	public void setNumeroActual(Integer numeroActual) {
		this.numeroActual = numeroActual;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getStr() {
		return numero + " (" + numeroDesde + " - " + numeroHasta + ")";
	}

}
