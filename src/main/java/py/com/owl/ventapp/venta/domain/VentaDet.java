package py.com.owl.ventapp.venta.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.producto.domain.Iva;
import py.com.owl.ventapp.producto.domain.SubProducto;

@Entity
@Audited
public class VentaDet extends GenericEntity {

	private static final String SECUENCIA = "ventadet_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaDet_subproducto_fk"))
	@NotNull(message = "{ventaDet.subproducto.notNull}")
	private SubProducto subproducto;

	/* si no usamos jsonignore va a traer cabecera por cada detalle */
	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaDet_ventacab_fk"))
	private VentaCab ventaCab;

	private float cantidad;

	private BigDecimal precio = BigDecimal.ZERO;

	private BigDecimal montototal = BigDecimal.ZERO;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaDet_iva_fk"))
	@NotNull(message = "{ventaDet.iva.notNull}")
	private Iva iva;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventaDet_empresa_fk"))
	@NotNull(message = "{ventaDet.empresa.notNull}")
	private Empresa empresa;

	public VentaDet() {
		empresa = new Empresa();
		ventaCab = new VentaCab();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public SubProducto getSubproducto() {
		return subproducto;
	}

	public void setSubproducto(SubProducto subproducto) {
		this.subproducto = subproducto;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getMontototal() {
		return montototal;
	}

	public void setMontototal(BigDecimal montototal) {
		this.montototal = montototal;
	}

	public Iva getIva() {
		return iva;
	}

	public void setIva(Iva iva) {
		this.iva = iva;
	}

	public VentaCab getVentaCab() {
		return ventaCab;
	}

	public void setVentaCab(VentaCab ventaCab) {
		this.ventaCab = ventaCab;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		// TODO Auto-generated method stub
		this.empresa = empresa;

	}

	@Override
	public String toString() {
		return "VentaDet [id=" + id + ", cantidad=" + cantidad + ", precio=" + precio + ", montototal=" + montototal
				+ "]";
	}

}
