package py.com.owl.ventapp.venta.controllers.forms;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import py.com.owl.ventapp.venta.domain.VentaCab;
import py.com.owl.ventapp.venta.domain.VentaDet;
import py.una.cnc.htroot.core.util.PersistResponse;
import py.una.cnc.lib.core.util.AppLogger;

@Controller
@RestController
public class VentaController {

	// @Qualifier("mvcValidator")
	@Autowired
	private SmartValidator smartValidator;

	private AppLogger logger = new AppLogger(getClass());

	@GetMapping("/prueba-venta/crear")
	@ResponseBody
	public ResponseEntity<VentaCab> crear(@Valid VentaCab ventaCab, BindingResult br) {

		logger.info("Creando Venta", ventaCab);
		boolean detallesInvalidos = false;

		// para guardar los resultados de valicadion de cada ventaDet
		List<PersistResponse<VentaDet>> ventaDetListResponse = new ArrayList<>();

		/*
		 * iteramos por cada detalle para hacer la validacion de cada ventaDet
		 * con smartvalidator se crea y se valida el resultado de la validacion
		 * se guarda en brDet
		 */
		for (VentaDet ventaDet : ventaCab.getVentaDetList()) {
			/* se guardan resultados de validacion */
			BindingResult brDet = new BeanPropertyBindingResult(ventaDet, "ventaDet");
			smartValidator.validate(ventaDet, brDet);
			logger.info("Validando ventaDet: {}, errores: {}", ventaDet, brDet.getFieldError());

			/* si tiene errores */

			/*
			 * se instancia pr y por cada registro agregamos
			 * ventadetlistResponse envolver ventaDet y los resultados de
			 * validacion por ejemplo detalle 5 tiene "estos errores"
			 */
			PersistResponse<VentaDet> resp = new PersistResponse<>();
			resp.setData(ventaDet);
			if (brDet.hasErrors()) {
				detallesInvalidos = true;
				resp.setSuccess(false);
				resp.setInfoMessage("Errores de validacion: " + brDet.getFieldErrorCount());

			} else {
				resp.setSuccess(true);
			}

			/* se agrega a nuestra lista */
			ventaDetListResponse.add(resp);
		}

		/* finalmente pasa a formr parte de ventacab */
		ventaCab.setVentaDetListResponse(ventaDetListResponse);

		if (br.hasErrors()) {
			return ResponseEntity.badRequest().body(ventaCab);
		} else {
			return ResponseEntity.ok(ventaCab);
		}
	}

}
