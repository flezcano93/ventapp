package py.com.owl.ventapp.venta.controllers.lists;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.venta.bc.VentaCabBC;
import py.com.owl.ventapp.venta.domain.VentaCab;
import py.una.cnc.htroot.core.bc.BusinessController;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@Scope("session")
@RequestMapping("/venta")
public class VentaCabList extends BaseList<VentaCab> {

	@Autowired
	private VentaCabBC ventaCabBC;

	@Override
	public BusinessController<VentaCab> getBusinessController() {
		// TODO Auto-generated method stub
		return ventaCabBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "fechaVenta", "numdoc", "cliente.codigo", "cliente.nombre", "cliente.apellido",
				"montoTotal" };

	}

	/* columnas filtrables, que no tienen valores nulos */
	@Override
	protected String getFilterableColumns() {
		return "fechaVenta||cliente.codigo||cliente.nombre||cliente.apellido";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<VentaCab> getJson() {
		return ventaCabBC.findEntities();

	}

	/*
	 * HorizontalAlign para indicar hacia donde quiero la columna Reportes
	 */
	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = (new ReportColumn("fechaVenta", 70, "dd/MM/yyyy", HorizontalAlign.CENTER));
		columnas.add(cod);
		columnas.add(new ReportColumn("cliente.nombre", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("cliente.apellido", 100, HorizontalAlign.LEFT));
		columnas.add(new ReportColumn("cliente.codigo", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("numdoc", 100));
		return columnas;
	}

	@Override
	protected Page getPage() {
		// return super.getPage();
		return Page.Page_A4_Landscape();
	}

}
