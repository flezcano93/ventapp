package py.com.owl.ventapp.venta.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.venta.domain.VentaCab;

public interface VentaCabDao extends BaseDao<VentaCab> {

}
