package py.com.owl.ventapp.venta.controllers.lists;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.venta.bc.VentaDetBC;
import py.com.owl.ventapp.venta.domain.VentaDet;
import py.una.cnc.htroot.core.bc.BusinessController;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@Scope("session")
@RequestMapping("/ventadet")
public class VentaDetList extends BaseList<VentaDet> {

	@Autowired
	private VentaDetBC ventaDetBC;

	@Override
	public BusinessController<VentaDet> getBusinessController() {
		// TODO Auto-generated method stub
		return ventaDetBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "montototal", "precio", "cantidad" };

	}

	/* columnas filtrables, que no tienen valores nulos */
	@Override
	protected String getFilterableColumns() {
		return "montototal||precio||cantidad";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<VentaDet> getJson() {
		return ventaDetBC.findEntities();

	}

	/*
	 * HorizontalAlign para indicar hacia donde quiero la columna Reportes
	 */
	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = (new ReportColumn("fechaVenta", 70, "dd/MM/yyyy", HorizontalAlign.CENTER));
		columnas.add(cod);
		columnas.add(new ReportColumn("montototal", 100, HorizontalAlign.LEFT));
		columnas.add(new ReportColumn("precio", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("cantidad", 100));
		return columnas;
	}

	@Override
	protected Page getPage() {
		// return super.getPage();
		return Page.Page_A4_Landscape();
	}

}
