package py.com.owl.ventapp.venta.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.venta.domain.VentaDet;

public interface VentaDetBC extends BaseBC<VentaDet> {

}
