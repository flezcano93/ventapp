package py.com.owl.ventapp.my.service;

import java.util.List;

import py.com.owl.ventapp.my.domain.Persona;

//BC
public interface PersonaService {

	void create(Persona persona);

	public List<Persona> getList();

}
