package py.com.owl.ventapp.my.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.my.domain.Persona;
import py.com.owl.ventapp.my.service.PersonaService;
import py.com.owl.ventapp.util.Log;

@RestController
@SessionScope
public class PersonaController {

	@Autowired
	private EntityManager em;

	@Autowired
	private PersonaService personaService;

	private Log log = new Log(getClass());

	// @RequestMapping(value="/persona",method=RequestMethod.POST)

	@PostMapping("/persona")
	// @GetMapping("/persona")

	/* permite el guardado del registro en la base de datos */
	public ResponseEntity<Persona> crear(@Valid Persona persona, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {

			log.info("Errores de validacion: {}", bindingResult.getFieldError());

			/* para que retorne el error de validacion */
			return ResponseEntity.badRequest().body(persona);
		} else {
			try {
				personaService.create(persona);
				return new ResponseEntity<>(persona, HttpStatus.CREATED);
			} catch (Exception ex) {
				log.error("Ocurrio un error : {}", ex);
				return new ResponseEntity<>(persona, HttpStatus.INTERNAL_SERVER_ERROR);

			}
		}

	}

	// persona?id=1 | persona/1
	@DeleteMapping("/persona/{id}")
	@Transactional
	public ResponseEntity<Persona> borrar(@PathVariable Long id) {

		Persona per = em.find(Persona.class, id);

		if (per == null) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		} else {
			em.remove(per);
			return ResponseEntity.ok(per);
		}

	}

	@SuppressWarnings("unchecked")
	@GetMapping("/persona")
	public ResponseEntity<List<Persona>> list() {
		try {
			return ResponseEntity.ok(personaService.getList());
		} catch (Exception ex) {
			log.error("No se pudo listar personas", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		/*
		 * String sql = "SELECT object (P) FROM Persona AS P"; Query query =
		 * em.createQuery(sql); return ResponseEntity.ok(query.getResultList());
		 */
	}

}
