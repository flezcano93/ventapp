package py.com.owl.ventapp.my.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.my.dao.PersonaDao;
import py.com.owl.ventapp.my.domain.Persona;

@Service
@SessionScope
public class PersonaServiceImpl implements PersonaService {

	/* no envio sql le llamo a personadao */
	@Autowired
	private PersonaDao personaDao;

	@Override
	@Transactional
	public void create(Persona persona) {
		personaDao.create(persona);
		// enviar email
	}

	@Override
	@Transactional
	public List<Persona> getList() {
		// TODO Auto-generated method stub
		return personaDao.getList();
	}

}
