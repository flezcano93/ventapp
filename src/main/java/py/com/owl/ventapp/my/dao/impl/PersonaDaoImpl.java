package py.com.owl.ventapp.my.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.my.dao.PersonaDao;
import py.com.owl.ventapp.my.domain.Persona;

@Repository
@SessionScope
public class PersonaDaoImpl implements PersonaDao {

	@Autowired
	private EntityManager em;

	@Override
	public void create(Persona persona) {
		em.persist(persona);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getList() {
		String sql = "SELECT object (P) FROM Persona AS P";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

}
