package py.com.owl.ventapp.my.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.domain.Empresa;

//quiero que sea una tabla y no una clase normal, le agrego entity
@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "persona_cedula_uk", columnNames = { "empresa_id", "cedula" }), })

public class Persona {

	// para generar id sin que el usuario cargue
	private static final String SECUENCIA = "persona_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)

	private Long id;
	@NotNull(message = "{persona.codigo.notNull}")
	@NotBlank(message = "{persona.codigo.notBlank}")
	@Size(max = 10, message = "{persona.codigo.size}")
	private String cedula;

	@NotNull(message = "{persona.nombre.notNull}")
	@NotBlank(message = "{persona.nombre.notBlank}")
	@Size(max = 100, message = "{persona.nombre.size}")
	private String nombre;

	@NotNull(message = "{persona.apellido.notNull}")
	@NotBlank(message = "{persona.apellido.notBlank}")
	@Size(max = 100, message = "{persona.apellido.size}")
	private String apellido;

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "{persona.empresa.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "persona_empresa_fk"))
	private Empresa empresa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}