package py.com.owl.ventapp.my.dao;

import java.util.List;

import py.com.owl.ventapp.my.domain.Persona;

public interface PersonaDao {

	void create(Persona persona);

	List<Persona> getList();

}
