package py.com.owl.ventapp.producto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.SubProductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.SubProducto;
import py.com.owl.ventapp.producto.service.SubProductoService;
import py.una.cnc.htroot.core.dao.Dao;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SubProductoServiceImpl extends BaseBCImpl<SubProducto> implements SubProductoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SubProductoDao subproductoDao;

	/*
	 * @Override public SubProductoDao getDAOInstance() { return subproductoDao;
	 * }
	 */

	@Override
	public Dao<SubProducto> getDAOInstance() {
		return subproductoDao;
	}

	@Override
	@Transactional
	public SubProducto buscarGeneradoPorProducto(Producto producto) {

		return subproductoDao.buscarGeneradoPorProducto(producto);
	}

}
