package py.com.owl.ventapp.producto.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.SubProductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.SubProducto;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SubProductoDaoImpl extends BaseDaoImpl<SubProducto> implements SubProductoDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<SubProducto> findEntities() {
		String sql = "SELECT object(P) FROM SubProducto AS P";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

	/* sin reutilizar ningun metodo */
	@Override
	public SubProducto buscarGeneradoPorProducto(Producto producto) {
		String jpql = "SELECT object (S) FROM SupProducto AS S WHERE producto_id = ?1 and generado is TRUE";
		/* getentitymanager hace lo mismo que declarar em */
		Query query = getEntityManager().createQuery(jpql);
		query.setParameter(1, producto.getId());
		try {
			/*
			 * singleresult por que necesitamos un solo registro por ejemplo si
			 * tengo timbrado me tiene que traer un solo resultado no puede
			 * traerme varios detalles
			 */
			return (SubProducto) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public SubProducto buscarGeneradoPorProducto2(Producto producto) {

		return findEntityByCondition("WHERE producto_id = ?1 " + "AND generado is TRUE", producto.getId());
	}

}
