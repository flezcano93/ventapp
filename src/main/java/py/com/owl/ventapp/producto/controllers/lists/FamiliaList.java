package py.com.owl.ventapp.producto.controllers.lists;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.producto.domain.Familia;
import py.com.owl.ventapp.producto.service.FamiliaService;
import py.una.cnc.htroot.core.bc.BusinessController;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@Scope("session")
@RequestMapping("/familia")
public class FamiliaList extends BaseList<Familia> {

	@Autowired
	private FamiliaService familiaService;

	@Override
	public BusinessController<Familia> getBusinessController() {
		// TODO Auto-generated method stub
		return familiaService;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "codigo", "nombre" };
	}

	@Override
	protected String getFilterableColumns() {
		return "codigo||nombre";
	}

	@RequestMapping("/json")
	@ResponseBody
	public java.util.List<Familia> getJson() {
		return familiaService.findEntities();

	}

	/* HorizontalAlign para indicar hacia donde quiero la columna */
	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = (new ReportColumn("codigo", 70, HorizontalAlign.CENTER));
		columnas.add(cod);
		columnas.add(new ReportColumn("nombre", 100, HorizontalAlign.RIGHT));
		return columnas;
	}

}
