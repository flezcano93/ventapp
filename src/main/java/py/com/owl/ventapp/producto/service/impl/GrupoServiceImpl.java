package py.com.owl.ventapp.producto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.GrupoDao;
import py.com.owl.ventapp.producto.domain.Grupo;
import py.com.owl.ventapp.producto.service.GrupoService;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class GrupoServiceImpl extends BaseBCImpl<Grupo> implements GrupoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private GrupoDao grupoDao;

	@Override
	public GrupoDao getDAOInstance() {
		return grupoDao;
	}

}
