package py.com.owl.ventapp.producto.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.producto.controllers.lists.ProductoList;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.service.FamiliaService;
import py.com.owl.ventapp.producto.service.GrupoService;
import py.com.owl.ventapp.producto.service.IvaService;
import py.com.owl.ventapp.producto.service.LineaService;
import py.com.owl.ventapp.producto.service.ProductoService;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@SessionScope
@RequestMapping("/producto")
public class ProductoForm extends BaseForm<Producto> {

	@Autowired
	private ProductoService productoService;

	@Autowired
	private FamiliaService familiaService;

	@Autowired
	private LineaService lineaService;

	@Autowired
	private GrupoService grupoService;

	@Autowired
	private ProductoList productoList;

	@Autowired
	private IvaService ivaService;

	@Override
	public BusinessController<Producto> getBusinessController() {
		// TODO Auto-generated method stub
		return productoService;
	}

	@Override
	protected ProductoList getListController() {

		return productoList;

	}

	/* donde esta mi template */
	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/producto/producto_index";

	}

	@Override
	protected void addExtraAttributes(Producto bean, ModelMap modelMap) {
		// TODO Auto-generated method stub

		modelMap.addAttribute("familiaList", familiaService.findEntities());
		modelMap.addAttribute("grupoList", grupoService.findEntities());
		modelMap.addAttribute("lineaList", lineaService.findEntities());
		modelMap.addAttribute("ivaList", ivaService.findEntities());
		super.addExtraAttributes(bean, modelMap);
	}

}
