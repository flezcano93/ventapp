package py.com.owl.ventapp.producto.service;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Producto;

public interface ProductoService extends BaseBC<Producto> {

}
