package py.com.owl.ventapp.producto.service.impl;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.ProductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.SubProducto;
import py.com.owl.ventapp.producto.service.ProductoService;
import py.com.owl.ventapp.producto.service.SubProductoService;
import py.una.cnc.htroot.core.dao.Dao;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class ProductoServiceImpl extends BaseBCImpl<Producto> implements ProductoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private ProductoDao productoDao;

	@Autowired
	private SubProductoService subProductoService;

	@Override
	public Dao<Producto> getDAOInstance() {

		return productoDao;
	}

	// @Override
	// @Transactional
	// public void create(Producto producto) {
	// /* ctrlsubproduco es un boolean entonces se asi de esta forma */
	// if (BooleanUtils.isTrue(producto.getCtrlSubproducto())) {
	// /* le llama a la superclase para crear */
	// super.create(producto);
	// } else {
	// super.create(producto);
	// /* debe generar subproducto */
	// SubProducto sp = new SubProducto();
	// sp.setProducto(producto);
	// sp.setPrecio(producto.getPrecio());
	// /*
	// * le llamo a la logica de negocio de subproducto para que me cree
	// */
	// subProductoService.create(sp);
	// }

	// }

	@Override
	@Transactional
	public void create(Producto producto) {
		super.create(producto);
		/*
		 * ctrlsubproduco es un boolean entonces se asi de esta forma si no
		 * controla genera un subproducto
		 */
		if (BooleanUtils.isFalse(producto.getCtrlSubproducto())) {
			/* debe generar subproducto */
			crearSub(producto);
		}

	}

	@Override
	@Transactional
	public void edit(Producto producto) {
		/*
		 * se debe generar subproducto pero solo si ya no existe si no controla
		 * genera uno por defecto si es que no existe con el
		 * buscarGeneradoporProducto
		 */
		if (BooleanUtils.isFalse(producto.getCtrlSubproducto())) {

			SubProducto sub = subProductoService.buscarGeneradoPorProducto(producto);
			if (sub == null) {
				crearSub(producto);
			} else { /* si a existe actualiza */
				sub.setPrecio(producto.getPrecio());
				subProductoService.edit(sub);
			}
		}
	}

	private void crearSub(Producto producto) {
		SubProducto sp = new SubProducto();
		sp.setProducto(producto);
		sp.setPrecio(producto.getPrecio());
		sp.setGenerado(Boolean.TRUE);
		subProductoService.create(sp);

	}

}
