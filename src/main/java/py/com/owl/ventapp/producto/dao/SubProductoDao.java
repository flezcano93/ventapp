package py.com.owl.ventapp.producto.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.SubProducto;

public interface SubProductoDao extends BaseDao<SubProducto> {

	/*
	 * en el dao por que no va a tener logica de negocios solo va a mostrar
	 * informacion generado por que va a ser unico no insertado por el usuario
	 */
	/* en el dao todas las operaciones que involucre a la base de datos */
	SubProducto buscarGeneradoPorProducto(Producto producto);

	SubProducto buscarGeneradoPorProducto2(Producto producto);
}
