package py.com.owl.ventapp.producto.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.LineaDao;
import py.com.owl.ventapp.producto.domain.Linea;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class LineaDaoImpl extends BaseDaoImpl<Linea> implements LineaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Linea> findEntities() {
		String sql = "SELECT object(L) FROM Linea AS L";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}

}
