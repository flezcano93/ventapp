package py.com.owl.ventapp.producto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.IvaDao;
import py.com.owl.ventapp.producto.domain.Iva;
import py.com.owl.ventapp.producto.service.IvaService;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class IvaServiceImpl extends BaseBCImpl<Iva> implements IvaService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private IvaDao ivaDao;

	@Override
	public IvaDao getDAOInstance() {
		return ivaDao;
	}

}
