package py.com.owl.ventapp.producto.service;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.SubProducto;

public interface SubProductoService extends BaseBC<SubProducto> {

	SubProducto buscarGeneradoPorProducto(Producto producto);
}
