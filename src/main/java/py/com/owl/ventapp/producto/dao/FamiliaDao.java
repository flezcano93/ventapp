package py.com.owl.ventapp.producto.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Familia;

public interface FamiliaDao extends BaseDao<Familia> {

}
