package py.com.owl.ventapp.producto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.LineaDao;
import py.com.owl.ventapp.producto.domain.Linea;
import py.com.owl.ventapp.producto.service.LineaService;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class LineaServiceImpl extends BaseBCImpl<Linea> implements LineaService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private LineaDao lineaDao;

	@Override
	public LineaDao getDAOInstance() {
		return lineaDao;
	}

}
