package py.com.owl.ventapp.producto.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.producto.controllers.lists.FamiliaList;
import py.com.owl.ventapp.producto.domain.Familia;
import py.com.owl.ventapp.producto.service.FamiliaService;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@Scope("session")
@RequestMapping("/familia")
public class FamiliaForm extends BaseForm<Familia> {

	@Autowired
	private FamiliaService familiaService;

	@Autowired
	private FamiliaList familiaList;

	@Override
	public BusinessController<Familia> getBusinessController() {
		// TODO Auto-generated method stub
		return familiaService;
	}

	@Override
	protected FamiliaList getListController() {

		return familiaList;

	}

	/* donde esta mi template */
	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/familia/familia_index";

	}

}
