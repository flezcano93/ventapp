package py.com.owl.ventapp.producto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.dao.FamiliaDao;
import py.com.owl.ventapp.producto.domain.Familia;
import py.com.owl.ventapp.producto.service.FamiliaService;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class FamiliaServiceImpl extends BaseBCImpl<Familia> implements FamiliaService {

	private static final long serialVersionUID = 1L;

	@Autowired
	private FamiliaDao familiaDao;

	@Override
	public FamiliaDao getDAOInstance() {
		return familiaDao;
	}

}
