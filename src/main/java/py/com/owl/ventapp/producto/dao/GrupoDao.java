package py.com.owl.ventapp.producto.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Grupo;

public interface GrupoDao extends BaseDao<Grupo> {

}
